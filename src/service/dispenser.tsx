import { MessageProps } from '../types'
import { useState } from 'react'

export default function dispenser(messages: MessageProps[]): MessageProps[] {
  const [list, setList] = useState<MessageProps[]>([])

  for (let i = 0; i < messages.length; i += 1) {
    setTimeout(() => {
      setList([...list, messages[i]])
      console.log(i)
    }, 1000)
  }

  return list
}
