import baseStyled, { createGlobalStyle, ThemedStyledInterface, ThemeProvider, keyframes } from 'styled-components'
import { normalize } from 'styled-normalize'

export const lightTheme = {
  color: {
    lightAccent: '#FFFFFF',
    lightShade: '#ACACAC',
    content: '#3D3D3D',
    darkAccent: '#191919',
    darkShade: '#262626',
    darkFaded: '#4B4B4B',
    variant: '#BE3A84',
    alternative: '#DD6C43',
    primary: '#D33B72',
  },
  shadow: '0px 3px 6px 0px rgba(0,0,0,0.16)',
}

export const darkTheme = {
  color: {
    lightAccent: '#FFFFFF',
    lightShade: '#ACACAC',
    content: '#3D3D3D',
    darkAccent: '#191919',
    darkShade: '#262626',
    darkFaded: '#4B4B4B',
    variant: '#BE3A84',
    alternative: '#DD6C43',
    primary: '#D33B72',
  },
  shadow: '0px 3px 6px 0px rgba(0,0,0,0.16)',
}

const GlobalStyle = createGlobalStyle`
  ${normalize}
  * {
    font-family: 'Montserrat', sans-serif;
    font-size: 14px;
    font-weight: 500;
  }
`

export default GlobalStyle
export { ThemeProvider, keyframes }
export type Theme = typeof lightTheme
export const styled = baseStyled as ThemedStyledInterface<Theme>
