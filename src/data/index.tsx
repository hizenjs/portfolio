import React from 'react'
import { Button, Gallery, Typography } from '../components'

const { Text, Strong } = Typography

const data = {
  messages: [
    {
      title: `Hi, I'm Matthieu "${'Hizen'}"`,
      content: (
        <Text>
          I work as a <Strong>Designer</Strong> & <Strong>Front-end Developer</Strong>. I am ready to support your
          designs with 4+ years of experience.
        </Text>
      ),
    },
    {
      title: 'What am I doing now?',
      content: <Text>Web engineer for Loyalty Company and Remote UI Designer as freelancer</Text>,
    },
    {
      title: 'See a sample of my work',
      content: (
        <Gallery
          list={[
            { source: 'https://i.imgur.com/OgGiRhb.png', name: 'abc' },
            { source: 'https://i.imgur.com/yUsbuHD.png', name: 'abc' },
            { source: 'https://i.imgur.com/EgxS6D9.png', name: 'abc' },
            { source: 'https://i.imgur.com/EgxS6D9.png', name: 'abc' },
          ]}
        />
      ),
    },
    {
      title: 'Get in touch',
      content: <Button name="Send main" />,
    },
  ],
  social: [
    { ref: 'https://www.instagram.com/hizen.js/', name: 'instagram' },
    { ref: 'https://dribbble.com/hizenjs', name: 'dribbble' },
    { ref: 'https://github.com/hizenjs', name: 'github' },
  ],
}

export default data
