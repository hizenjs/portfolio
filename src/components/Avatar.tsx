import React, { ReactElement } from 'react'
import { styled, keyframes } from '../theme'
import useWindowDimensions from '../hooks/useWindowDimensions'
import { ImageProps } from '../types'

const size = 54

const rotation = keyframes`
 0% { transform: rotate(1deg) }
 100% { transform: rotate(360deg) }
`

const Gradient = styled.div`
  align-items: center;
  animation: ${rotation} 2s infinite linear;
  background: linear-gradient(
    to left,
    ${(props) => props.theme.color.variant} 0%,
    ${(props) => props.theme.color.alternative} 100%
  );
  border-radius: ${size + 10}px;
  display: flex;
  height: ${size + 10}px;
  justify-content: center;
  margin-right: 12px;
  width: ${size + 10}px;
  @media (max-width: 588px) {
    margin-right: 0;
  }
`

const Image = styled.img`
  animation: ${rotation} 2s reverse infinite linear;
  background-color: ${(props) => props.theme.color.primary};
  border: 3px solid ${(props) => props.theme.color.darkAccent};
  border-radius: ${size}px;
  height: ${size}px;
  object-fit: cover;
  width: ${size}px;
  @media (max-width: 588px) {
    animation: none;
    border: none;
    height: 29px;
    width: 29px;
  }
  :hover {
    animation: ${rotation} infinite ease-in;
  }
`

const Avatar = (props: ImageProps): ReactElement => {
  const windowDimensions = useWindowDimensions()
  const { source } = props
  if (windowDimensions.width < 588) return <Image {...props} src={source} />
  return (
    <Gradient>
      <Image {...props} src={source} />
    </Gradient>
  )
}

export default Avatar
