import React, { ReactElement } from 'react'
import { styled, keyframes } from '../theme'
import { TypingProps } from '../types'

const reveal = keyframes`
  0% {
    height: 0;
    opacity: 0;
  }
  100% {
    opacity: 1;
    height: auto;
  }
`

const first = keyframes`
  0% {
    top: 4px;
  }
  22% {
    top: 0;
  }
  44% {
    top: 4px;
  }
`

const second = keyframes`
  0% {
    top: 4px;
  }
  44% {
    top: 0;
  }
  66% {
    top: 4px;
  }
`

const third = keyframes`
  0% {
    top: 4px;
  }
  66% {
    top: 0;
  }
  88% {
    top: 4px;
  }
`

const border: { [k: string]: string } = {
  first: '8px 8px 18px 18px',
  normal: '18px',
}

const Container = styled.div`
  animation: ${reveal};
  animation-duration: 300ms;
  background-color: ${(props) => props.theme.color.darkShade};
  border-radius: 18px;
  box-shadow: ${(props) => props.theme.shadow};
  margin-top: 8px;
  width: calc(80px - (20px * 2));
  padding: 14px 20px 20px 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: calc(314px - 80px);
`

const Dot = styled.div`
  background-color: ${(props) => props.theme.color.content};
  width: 8px;
  height: 8px;
  border-radius: 50%;
  position: relative;
  margin-right: 8px;
  top: 4px;
  :nth-child(3) {
    margin-right: 0px;
    animation: ${third} cubic-bezier(0.42, 0, 0.58, 1) 0.6s infinite;
  }
  :nth-child(2) {
    animation: ${second} cubic-bezier(0.42, 0, 0.58, 1) 0.6s infinite;
  }
  :nth-child(1) {
    animation: ${first} cubic-bezier(0.42, 0, 0.58, 1) 0.6s infinite;
  }
`

const Typing = ({ type }: TypingProps): ReactElement => (
  <Container style={type && { borderRadius: border[type] }}>
    <Dot />
    <Dot />
    <Dot />
  </Container>
)

export default Typing
