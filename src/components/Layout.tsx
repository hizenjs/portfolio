import React, { ReactElement } from 'react'
import pattern from '../assets/pattern.png'
import { styled } from '../theme'
import { LayoutProps } from '../types'

const Wrapper = styled.div`
  align-items: center;
  background-color: ${(props) => props.theme.color.darkAccent};
  background-image: url(${pattern});
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-height: calc(100vh - (34px * 2));
  padding: 34px;
  width: calc(100% - (34px * 2));
  @media (max-width: 588px) {
    min-height: calc(100vh - (12px * 2));
    padding: 12px;
    width: calc(100% - (12px * 2));
  }
`

const Layout = ({ children }: LayoutProps): ReactElement => <Wrapper>{children}</Wrapper>

export default Layout
