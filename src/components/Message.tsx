import React, { ReactElement } from 'react'
import { keyframes, styled } from '../theme'
import Typography from './Typography'
import { MessageProps } from '../types'

const { Title } = Typography

const reveal = keyframes`
  0% {
    height: 0;
    opacity: 0;
  }
  100% {
    opacity: 1;
    height: auto;
  }
`

const border: { [k: string]: string } = {
  first: '18px 18px 8px 8px',
  normal: '8px',
  last: '8px 8px 18px 18px',
}

const Container = styled.div`
  animation: ${reveal};
  animation-duration: 300ms;
  background-color: ${(props) => props.theme.color.darkShade};
  border-radius: 18px;
  box-shadow: ${(props) => props.theme.shadow};
  margin-top: 8px;
  max-width: calc(314px - (20px * 2));
  padding: 16px 20px;
  transition: 300ms;
`

const Message = ({ title, content, type }: MessageProps): ReactElement => (
  <Container style={type && { borderRadius: border[type] }}>
    <Title>{title}</Title>
    {content}
  </Container>
)

export default Message
