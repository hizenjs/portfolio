import React, { ReactElement } from 'react'
import Typography from './Typography'
import { styled } from '../theme'
import { FooterProps, ItemType } from '../types'

const { Link } = Typography

const Wrapper = styled.h1`
  align-items: center;
  display: flex;
  justify-content: flex-end;
  width: 100%;
  @media (max-width: 588px) {
    justify-content: space-around;
  }
`

const Footer = ({ social }: FooterProps): ReactElement => (
  <Wrapper>
    {social.map(({ ref, name }: ItemType, i: number) => (
      <Link key={i.toString()} href={ref}>
        {name}
      </Link>
    ))}
  </Wrapper>
)

export default Footer
