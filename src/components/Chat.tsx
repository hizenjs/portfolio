import React, { ReactElement, useEffect, useState } from 'react'
import { styled } from '../theme'
import Message from './Message'
import { Avatar } from './index'
import useWindowDimensions from '../hooks/useWindowDimensions'
import { ChatProps, MessageProps } from '../types'
import Typing from './Typing'

const Wrapper = styled.div`
  align-items: flex-end;
  display: flex;
  justify-content: center;
  height: 435px;
  transition: 300ms;
`
const List = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  transition: 300ms;
`

const Chat = ({ messages }: ChatProps): ReactElement => {
  const windowDimensions = useWindowDimensions()
  const [list, setList] = useState<MessageProps[]>([])
  const [count, updateCount] = useState<number>(0)
  const [isWriting, setWriting] = useState<boolean>(true)
  useEffect(() => {
    if (count < messages.length) {
      setTimeout(() => {
        setWriting(true)
      }, 2000)
      setTimeout(() => {
        setWriting(false)
        setList([...list, messages[count]])
        updateCount(count + 1)
      }, 4000)
    }
  }, [count])

  return (
    <Wrapper>
      {windowDimensions.width > 588 && <Avatar source="https://i.imgur.com/SqrrfgS.png" />}
      <List>
        {list.map(({ title, content }: MessageProps, i: number) => (
          <Message
            key={i.toString()}
            title={title}
            content={content}
            type={i === 0 ? 'first' : i === messages.length - 1 ? 'last' : 'normal'}
          />
        ))}
        {isWriting && <Typing type={count < 1 ? 'normal' : 'first'} />}
      </List>
    </Wrapper>
  )
}

export default Chat
