import React, { ReactElement } from 'react'
import { styled } from '../theme'
import { Avatar } from './index'
import useWindowDimensions from '../hooks/useWindowDimensions'
import { BrandProps } from '../types'

const Wrapper = styled.div`
  align-items: flex-start;
  display: flex;
  justify-content: space-between;
  width: 100%;
  @media (max-width: 588px) {
    margin-top: 18px;
    width: calc(100% - (36px * 2));
    justify-content: space-between;
  }
`

const Name = styled.h1`
  color: ${(props) => props.theme.color.lightAccent};
  font-size: 14px;
`

const Brand = ({ title = 'original' }: BrandProps): ReactElement => {
  const windowDimensions = useWindowDimensions()
  return (
    <Wrapper>
      <Name>{title}</Name>
      {windowDimensions.width < 588 && <Avatar source="https://i.imgur.com/SqrrfgS.png" />}
    </Wrapper>
  )
}
export default Brand
