import { styled } from '../theme'

const Text = styled('p')`
  color: ${(p) => p.theme.color.lightShade};
  margin: 0;
`

const Title = styled('h2')`
  align-items: center;
  color: ${(p) => p.theme.color.lightAccent};
  display: flex;
  margin: 0;
  padding-bottom: 5px;
`

const Strong = styled('span')`
  color: ${(p) => p.theme.color.primary};
`

const Link = styled('a')`
  color: ${(p) => p.theme.color.darkFaded};
  cursor: pointer;
  margin-left: 44px;
  text-decoration: none;
  @media (max-width: 588px) {
    margin-left: 0;
  }
`

const Typography = {
  Link,
  Text,
  Title,
  Strong,
}

export default Typography
