import React, { ReactElement } from 'react'
import { FiMail } from 'react-icons/fi'
import { styled } from '../theme'
import Typography from './Typography'
import { ButtonProps } from '../types'

const { Title } = Typography

const Container = styled.div`
  align-items: flex-end;
  background-color: ${(props) => props.theme.color.content};
  border-radius: 99px;
  display: flex;
  justify-content: flex-start;
  padding: 11px 20px 8px 20px;
`

const Icon = styled(FiMail)`
  color: ${(props) => props.theme.color.lightAccent};
  font-size: 20px;
  margin-right: 6px;
`

const Button = ({ name }: ButtonProps): ReactElement => (
  <Container onClick={() => alert('Contact')}>
    <Title>
      <Icon />
      {name}
    </Title>
  </Container>
)

export default Button
