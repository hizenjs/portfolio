import React, { ReactElement } from 'react'
import { styled } from '../theme'
import Typography from './Typography'
import { GalleryProps, ImageType } from '../types'

const { Title } = Typography

const List = styled.div`
  align-items: center;
  display: flex;
  justify-content: flex-start;
`

const Image = styled.img`
  background-color: ${(props) => props.theme.color.primary};
  border-radius: 7px;
  height: 67px;
  margin-right: 6px;
  object-fit: cover;
  width: 90px;
`

const More = styled.div`
  align-items: center;
  background-color: ${(props) => props.theme.color.content};
  border-radius: 7px;
  display: flex;
  height: calc(67px - (15px * 2));
  justify-content: center;
  padding: 15px 7px;
  text-align: center;
  width: 90px;
  cursor: pointer;
`

const Gallery = ({ list }: GalleryProps): ReactElement => {
  const preview: boolean = list.length > 2
  return (
    <List>
      {list.map(({ source }: ImageType, i: number) => i < 2 && <Image key={i.toString()} src={source} />)}
      {preview && (
        <More onClick={() => alert('Show all')}>
          <Title>
            +{list.length - 2}
            <br />
            Show all
          </Title>
        </More>
      )}
    </List>
  )
}

export default Gallery
