import { ReactNode } from 'react'

export type ItemType = { ref: string; name: string }
export type ImageType = { source: string; name: string }

export interface ImageProps {
  source?: string
}

export interface BrandProps {
  title: string
}

export interface ButtonProps {
  name: string
}

export interface ChatProps {
  messages: MessageProps[]
}

export interface FooterProps {
  social: ItemType[]
}

export interface GalleryProps {
  list: ImageType[]
}

export interface LayoutProps {
  children?: ReactNode
}

export interface MessageProps {
  title: string
  content: ReactNode
  type?: 'first' | 'normal' | 'last'
}

export interface TypingProps {
  type?: 'first' | 'normal'
}
