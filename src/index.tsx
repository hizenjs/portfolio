import React from 'react'
import ReactDOM from 'react-dom'
import GlobalStyle, { ThemeProvider, darkTheme } from './theme'
import { Brand, Chat, Footer, Layout } from './components'
import data from './data'

ReactDOM.render(
  <React.StrictMode>
    <GlobalStyle />
    <ThemeProvider theme={darkTheme}>
      <Layout>
        <Brand title="only good dev" />
        <Chat messages={data.messages} />
        <Footer social={data.social} />
      </Layout>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root'),
)
